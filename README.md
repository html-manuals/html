<div align="center">
  <h2>HTML</h2>
  <p>Los datos se convirtieron en la nueva materia prima de los negocios. <b>Privacidad y Protección de Datos. #DataPrivacity</b></p>
</div>

<div align="center">
  <a href="https://dataprivacity.com/" target="_blank">
    <img src="assets/images/logo.png" width="200">
  </a>
  <h1>@DataPrivacity</h1>
</div>

<div align="center">
  <p>Aprende en nuestras redes:</p>
  <a href="https://DataPrivacity.com/" target="_blank">
    <img src="assets/images/globe.svg" width="50">
  </a>
  <a href="https://twitter.com/DataPrivacity" target="_blank">
    <img src="assets/images/twitter.svg" width="50">
  </a>
  <a href="https://instagram.com/DataPrivacity" target="_blank">
    <img src="assets/images/instagram.svg" width="50">
  </a>
  <a href="https://www.facebook.com/DataPrivacity/" target="_blank">
    <img src="assets/images/facebook.svg" width="50">
  </a>
  <a href="https://youtube.com/" target="_blank">
    <img src="assets/images/youtube.svg" width="50">
  </a>
  <a href="https://linkedin.com/company/DataPrivacity" target="_blank">
    <img src="assets/images/linkedin.svg" width="50">
  </a>
</div>

<div align="center">
  <p>Alojamos proyectos en:</p>
  <a href="https://gitlab.com/DataPrivacity" target="_blank">
    <img src="assets/images/gitlab.svg" width="50">
  </a>
  <a href="https://github.com/DataPrivacity" target="_blank">
    <img src="assets/images/github.svg" width="50">
  </a>
    <a href="https://drive.google.com/drive/folders/1uHOoUbx83PSKySfbxBOqwlthUA6ofHYP?usp=sharing" target="_blank">
      <img src="assets/images/download.svg" width="50">
  </a>
</div>

<div align="center">
  <p>DR, Cofundador de: DataPrivacity</p>
  <a href="https://twitter.com/wiiiccho" target="_blank">
    <img src="assets/images/cofundador.png" width="50">
  </a>
</div>

Todo el contenido publicado es modificable, si tu quieres colaborar, ves errores o añadir contenido, puedes hacerlo; cumpliendo los siguientes requisitos:

<div align="center">
  <b>“Todos para uno, uno para todos.”</b>
</div>
<br>
<div align="center">
  <b>“Llegar juntos es el principio; mantenerse juntos es el progreso; trabajar juntos es el éxito.”</b>
</div>
<br>

<div align="center">
  <b>Mi trabajo en el software libre está motivado por un objetivo idealista: difundir libertad y cooperación. Quiero motivar la expansión del software libre, reemplazando el software privativo que prohíbe la cooperación, y de este modo hacer nuestra sociedad mejor. Richard Stallman</b>
</div>

# Tabla de contenido
- [Introduccion](#Introduccion)
    - [Front-end(cliente)](#Frontend)
    - [Back-End(servidor)](#Backend)
    - [Fullstack](#Fullstack)
    - [Páginas Estáticas vs. Dinámicas](#Páginas-Estáticas-vs-Dinámicas)
- [HTML](#HTML)
    - [Anatomía de un elemento](#Anatomía-de-un-elemento)
    - [Estructura de una página](#Estructura-de-una-página)
    - [HTML semantico](#HTML-semantico)
- [Multimedia](#Multimedia)
    - [Tipos de imágenes](#Tipos-de-imágenes)
    - [Elemento img](#Elemento-img)
    - [Elemento video](#Elemento-video)
- [Formulario](#Formulario)
    - [Elementos from, label y input](#Elementos-from-label-y-input)

# <a name="Introduccion">Introduccion</a>
## <a name="Frontend">Front-end(cliente)</a>

El desarrollo web Front-end consiste en la conversión de datos en una interfaz gráfica para que el usuario pueda ver e interactuar con la información de forma digital usando HTML, CSS y JavaScript.

- **Hyper Text Markup Language (HTML)**: HTML, siglas en inglés de Hyper Text Markup Language (lenguaje de marcas de hipertexto), es la columna vertebral de cualquier proceso de desarrollo web, sin el cual las páginas web no existirían. Hipertexto significa que el texto tiene enlaces, denominados hipervínculos, incrustados en el mismo. Cuando el usuario hace clic en una palabra o una frase que tiene un hipervínculo, se realiza una llamada a otra página web. El lenguaje de marcas indica que el texto puede convertirse en imágenes, tablas, enlaces, y otras representaciones. El código HTML es el que provee un marco general de cuál será el aspecto del sitio. HTML fue desarrollado por Tim Berners-Lee. La última versión de HTML se llama HTML5 y se publicó en 28 de octubre de 2014, por recomendación de W3C. Esta versión contiene nuevas y eficientes maneras de manejar elementos como archivos de vídeo y audio.
- **Cascading Style Sheets (CSS)**: Las hojas de estilo en cascada, en inglés Cascading Style Sheets, controlan la apariencia visual del sitio web y permiten darle un aspecto único. Esto se consigue con hojas de estilo que se sobreponen sobre otras reglas de estilo y se lanzan basándose en el tipo de entrada, como el tamaño y la resolución de la pantalla del dispositivo.
- **JavaScript**: JavaScript es un lenguaje de programación imperativa basado en eventos (a diferencia del modelo de programación declarativa de HTML) que se utiliza para transformar una página HTML estática en una interfaz dinámica.

## <a name="Backend">Back-End(servidor)</a>

Back-End es la parte o rama del desarrollo web encargada de que toda la lógica de una página funcione del lado del servidor. Consiste en el conjunto de acciones que pasan dentro de una web, pero que no podemos ver.

- **Principales lenguajes de programación de Back-End**: Python, Ruby, PHP, Java y Perl.
- **DevOps**: DevOps (acrónimo inglés de development -desarrollo- y operations -operaciones-) es un conjunto de prácticas que agrupan el desarrollo de software ( Dev ) y las operaciones de TI ( Ops ). Su objetivo es hacer más rápido el ciclo de vida del desarrollo de software y proporcionar una entrega continua de alta calidad. DevOps es una práctica complementaria al desarrollo de software ágil ; esto debido a que varias de las características de DevOps provienen de la metodología Agile (término en inglés para la metodologÍa de desarrollo ágil).
- **Base de datos**: Una base de datos es un conjunto de datos pertenecientes a un mismo contexto y almacenados sistemáticamente para su posterior uso.

## <a name="Fullstack">Fullstack</a>

Un Fullstack es la fucion de todas las cosas que hace un Frontend y un Frontend. “Un fullstack es el desarrollador que entiende bien cómo funciona un producto web de principio a fin”.

<div align="center">
  <img src="img/Fullstack/1.png" width="500">
</div>

## <a name="Páginas-Estáticas-vs-Dinámicas">Páginas Estáticas vs. Dinámicas</a>

<div align="center">
  <img src="img/paginas/Estaticas-vs-Dinamicas.png" width="500">
</div>

# <a name="HTML">HTML</a>
## <a name="Anatomía-de-un-elemento">Anatomía de un elemento</a>

<div align="center">
  <img src="assets/images/html/elemento.png" width="600">
</div>

#### Partes de un elemento:

HTML consiste en una serie de elementos que usarás para encerrar diferentes partes del contenido para que se vean o comporten de una determinada manera.

- [x] La etiqueta de apertura: consiste en el nombre del elemento (en este caso, a), encerrado por paréntesis angulares (< >) de apertura y cierre.
- [x] La etiqueta de cierre: es igual que la etiqueta de apertura, excepto que incluye una barra de cierre (/) antes del nombre del elemento.
- [x] El contenido: este es el contenido del elemento, que en este caso es sólo texto.
- [x] El elemento: la etiqueta de apertura, más la etiqueta de cierre, más el contenido equivale al elemento.

#### Partes de un atributo:

Los atributos contienen información adicional acerca del elemento.

- [x] El nombre del atributo, seguido por un signo de igual (=).
- [x] Comillas de apertura y de cierre, encerrando el valor del atributo.

## <a name="Estructura-de-una-página">Estructura de una página</a>

Este elemento se usa para especificar qué versión de HTML está usando el documento.

```html

<!DOCTYPE html>

```

Elemento HTML raiz) representa la raiz de un documento HTML. El atributo HTML lang especifica el idioma del contenido de texto utilizado en una página web.

```html

<html lang="es">
</html>

```

El elemento head provee información general (metadatos) acerca del documento, incluyendo su título y enlaces a scripts y hojas de estilos.

```html

<head>
</head>

```

Los elementos meta, sirven para aportar información sobre el documento. Este elemento simplemente especifica la codificación de caracteres del documento es decir, el conjunto de caracteres que el documento puede usar. utf-8 es un conjunto de caracteres universal que incluye casi todos los caracteres de casi cualquier idioma humano.

```html

<meta charset="UTF-8" />

```

Los elementos meta, sirven para aportar información sobre el documento. Este elemento meta description podemos describir brevemente el contenido de la página web. Esta información se muestra como snippet (una síntesis en dos líneas del tema de una página que aparece bajo la URL) en los buscadores de uso más generalizado como Google o Bing, por lo que se recomienda cuidar su redacción. Por su papel central en tanto que influye de manera decisiva en la elección del usuario del mejor resultado acorde con su búsqueda, este meta tag es considerado uno de los más importantes en cuanto a la optimización para los buscadores. La descripción no debe sobrepasar los 160 caracteres de longitud, ya que se corre el riesgo de que esta aparezca incompleta en la lista de resultados. La metadescripción se dirige principalmente al usuario, por lo que debería contener un resumen preciso del contenido que le espera cuando haga clic en la página. Según Google, cada subpágina de una página web debería contar con una descripción propia que facilite información útil y precisa sobre su contenido.

```html

<meta name="description" content="Describir el contenido de la página web"/>

```

Es la etiqueta por defecto de cualquier página web y no es necesario incluirla. Indica al rastreador que rastree, indexe y siga los enlaces contenidos en la página web. Es innecesaria porque éste es el comportamiento por defecto de cualquier robot.

```html

<meta name="robots"  content="index,follow"  />

```

La etiqueta title en HTML se usa para definir el título del documento HTML. Establece el título en la barra de herramientas del navegador. Proporciona el título de la página web cuando se agrega a favoritos. Muestra el título de la página en los resultados del motor de búsqueda. este no debe sobrepasar la longitud máxima de 55 caracteres que pueden mostrar los buscadores.

```html

<title>Mi página</title>

```

El Viewport es el área visible del usuario de una página web. La ventana gráfica varía según el dispositivo y será más pequeña en un teléfono móvil que en la pantalla de una computadora. Antes de las tabletas y los teléfonos móviles, las páginas web estaban diseñadas solo para pantallas de computadora, y era común que las páginas web tuvieran un diseño estático y un tamaño fijo.

- width=device-width establece el ancho de la página para seguir el ancho de pantalla del dispositivo (que variará según el dispositivo).
- initial-scale=1.0 establece el nivel de zoom inicial cuando el navegador carga la página por primera vez.

```html

<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

```

#### Ejemplo

```html

<!DOCTYPE html>
<html lang="es">

  <!-- Contendedor no visual para el usuario final-->
  <head>
    <meta charset="UTF-8" />
    <meta name="description" content="Describir el contenido de la página web"/>
    <title>Mi página</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>

  <!-- Contendedor visual para el usuario final-->
  <body>
  </body>
</html>

```

## <a name="HTML-semantico">HTML semantico</a>

Las etiquetas semánticas ayudan a la implementación de los estándares de la Web Semántica, cuya finalidad es hacer el contenido de internet legible para aplicaciones informáticas.

| Elemento | Descripción |
| ------ | ------ |
| header | Representa un grupo de ayudas introductorias o de navegación. También puede contener logos, formulario de búsqueda o tabla de contenidos. |
| nav | Sección de una página que enlaza a otras páginas o partes de la misma. Una sección con links de navegación. |
| aside | Representa una sección de una página que consiste en contenido que está indirectamente relacionado con el contenido principal del documento. |
| main | Representa el contenido predominante de la página. |
| article | Representa una sección de contenido que puede ser distribuido de forma independiente. |
| section | Representa a una sección genérica de una pagina. Agrupa contenidos que tienen una relación temática entre si. |
| footer | Representa un pie de página para el elemento que lo contiene. Generalmente contiene información acerca de quién lo escribió, enlaces a documentos relacionados, datos de derechos de autor o similares. |

#### Ejemplos

<div align="center">
  <img src="assets/images/html/html_semantico/semantico_0.png" width="600">
</div>

<div align="center">
  <img src="assets/images/html/html_semantico/semantico_1.png" width="600">
</div>

# <a name="Multimedia">Multimedia</a>
## <a name="Tipos-de-imágenes">Tipos de imágenes</a>

Una imagen (del latín imago) es una representación visual, que manifiesta la apariencia visual de un objeto real o imaginario.

#### Lossless: compresión sin pérdida

Con la compresión sin pérdida (lossless), cada bit que estaba originalmente en el archivo, se puede restaurar al aplicar la decodificación.

- [x] GIF
- [x] PNG 8
- [x] PNG 24
- [x] SVG

#### Lossy: compresión con pérdida

Por otro lado, la compresión con pérdida reduce un archivo al eliminar permanentemente cierta información. Naturalmente los algoritmos intentan preservar la mayor calidad posible, eliminando información duplicada o información que los sentidos no sean capaces de captar.

- [x] JPG
- [x] JPEG

#### Tabla de direfencias

<div align="center">
  <img src="assets/images/html/multimedia/multimedia.png" width="800">
</div>

#### Optimización de imágenes

- [x] El tamaño máximo recomendado para una imágen es de: 70 kB
- [x] [Comprime el tamaño de una imagen, para hacerla más ligera](https://tinypng.com/)
- [x] [Elimina los metadatos de una imagen, para reducir su tamaño](https://www.verexif.com/)
- [x] [Convierte jpg, png a svg](https://picresize.com/)

## <a name="Elemento-img">Elemento img</a>

```html

<figure>
  <img src="img/fideos.png" alt="Esto es una imagen de comida de fideos">
  <!-- Etiqueta para agregar una pequeña descripcion de la imagen -->
  <figcaption>Es una taza de fideos</figcaption>
</figure>

```

- [x] [Bancos de imagenes](https://www.pexels.com/es-es/)
- [x] [Bancos de imagenes](https://pixabay.com/es/)
- [x] [Bancos de imagenes](https://unsplash.com/)

## <a name="Elemento-video">Elemento video</a>

```html
<main>
  <section>
      <video  width="320" height="180" controls preload="auto" poster="img/fideos.png">
        <!-- atributo controls para que en el video aparezca los controles del video -->

        <!-- atributo preload="auto" para que el navegador empieze a
        descargar el video al momento de encontrar con la etiqueta video-->

        <!-- Enfoque en el tiempo en que minuto empieza y en que minuto queremos que deque de reproducirse
        se indica despues de el #t=inicio,final  representados en segundos-->
        <!-- <source src="img/Bird.mp4#t=10,11"/> -->

        <!-- Forma correcta de cargar un video de diferentes por formatos si un formato es
        admitido por el navegador ya no es necesario que repoduzca el resto de pormatos -->
        <source src="img/Bird.mp4"/>
        <source src="img/Bird.pdf"/>
        <source src="img/Bird.txt"/>
        <source src="img/Bird.html"/>
      </video>
  </section>
</main>
```

# <a name="Formulario">Formulario</a>
## <a name="Elementos-from-label-y-input">Elementos from, label y input</a>

El mejor formulario, es el que no existe.

> Codigo de la clase esta en la carpeta con nombre de clase-16

**URL**

- [input](https://developer.mozilla.org/es/docs/Web/HTML/Element/inputf)
- [autocomplete](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/autocomplete)